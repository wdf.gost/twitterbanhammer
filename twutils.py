import json
import tweepy
import time


def get_user_info(api, user_id):
    user = api.get_user(user_id)
    if user is None:
        return ''
    return user._json['name'] + '(' + user._json['screen_name'] + ')'


def write_list(items, filename):
    with open(filename, 'w') as outf:
        for item in items:
            outf.write(item + '\n')


def read_list(filename):
    res = []
    with open(filename, 'r') as outf:
        for line in outf:
            res.append(line[:-1])
    return res


def init_api():
    with open("twitter_credentials.json", "r") as file:
        creds = json.load(file)
        auth = tweepy.OAuthHandler(creds['consumer_key'],
                                   creds['consumer_secret'])
        auth.set_access_token(creds['access_token'],
                              creds['access_token_secret'])
    return tweepy.API(auth)


def limit_handled(cursor):
    while True:
        try:
            yield cursor.next()
        except tweepy.RateLimitError:
            print('Out of Rate limit - wait for 15 mins')
            for i in range(60):
                time.sleep(15)
                print('#' * i, '_' * (60 - i), end='\r')
            print('#' * 60)
        except:
            break


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    HEADER = '\033[30;47m'


def print_colored(text, bcolor, end='\n'):
    print(f"{bcolor}{text}{bcolors.ENDC}", end=end)
