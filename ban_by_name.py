import tweepy
import json
import sys
from twutils import *
from urllib.parse import quote


api = init_api()


white_list = set(read_list('friends.txt'))
black_list = set(read_list('blocked.txt'))
mute_list = set(read_list('muted.txt'))
grey_list = set(read_list('followers.txt'))

##############################################################


PATTERNS = [
    'part-of-name',
]


print('>Search by names')

try:
    for pats in PATTERNS:
        query = quote(pats)
        for item in limit_handled(tweepy.Cursor(api.search_users,
                                                q=query).items()):
            user = item._json['screen_name']

            print(user, end=' -> ')
            if user in black_list:
                print('already blocked')
                continue
            elif user in white_list:
                print_colored('skip', bcolors.FAIL)
                continue
            elif user in grey_list and user not in mute_list:
                grey_list.add(user)
                print_colored('mute', bcolors.WARNING)
                api.create_mute(user)
            else:
                black_list.add(user)
                print('block')
                api.create_block(user)
except:
    print('Something wrong')


##############################################################

write_list(list(mute_list), 'muted.txt')
write_list(list(black_list), 'blocked.txt')
