import tweepy
from twutils import *


api = init_api()

white_list = set()
for user in limit_handled(tweepy.Cursor(api.friends).items()):
    white_list.add(user._json['screen_name'])
print('Friends: ', len(white_list))
write_list(white_list, 'friends.txt')


grey_list = set()
for user in limit_handled(tweepy.Cursor(api.followers).items()):
    grey_list.add(user._json['screen_name'])
print('Followers: ', len(grey_list))
write_list(grey_list, 'followers.txt')


black_list = set()
for user in limit_handled(tweepy.Cursor(api.blocks).items()):
    black_list.add(user._json['screen_name'])
print('Blocked: ', len(black_list))
write_list(black_list, 'blocked.txt')


mute_list = set()
for user in limit_handled(tweepy.Cursor(api.mutes).items()):
    mute_list.add(user._json['screen_name'])
print('Muted: ', len(mute_list))
write_list(mute_list, 'muted.txt')
